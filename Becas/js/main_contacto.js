//VALIDACION DE DATOS formulario 1//

const formulario = document.getElementById('form');
const inputs = document.querySelectorAll('#form input');
const expresiones = {
	usuario: /^[a-zA-Z0-9\_\-]{4,16}$/, // Letras, numeros, guion y guion_bajo
	nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
	password: /^.{4,12}$/, // 4 a 12 digitos.
	correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
    telefono: /^\d{7,14}$/ // 7 a 14 numeros.
}

const validarFormulario = (e) =>{

    switch (e.target.name){

        case "usuario":
            if(expresiones.usuario.test(e.target.value)){

                document.getElementById('grupo_usuario').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_usuario').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo_usuario i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo_usuario i').classList.remove('fa-times-circle');
                document.querySelector('#grupo_usuario .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_usuario').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_usuario').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_usuario i').classList.add('fa-times-circle');
                document.querySelector('#grupo_usuario i').classList.remove('fa-check-circle');
                document.querySelector('#grupo_usuario .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;
        case "correo":
            if(expresiones.correo.test(e.target.value)){

                document.getElementById('grupo_correo').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_correo').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo_correo i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo_correo i').classList.remove('fa-times-circle');
                document.querySelector('#grupo_correo .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_correo').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_correo').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_correo i').classList.add('fa-times-circle');
                document.querySelector('#grupo_correo i').classList.remove('fa-check-circle');
                document.querySelector('#grupo_correo .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;

    case "telefono":
        if(expresiones.telefono.test(e.target.value)){

            document.getElementById('grupo_telefono').classList.remove('formulario__grupo-incorrecto');
            document.getElementById('grupo_telefono').classList.add('formulario__grupo-correcto');
            document.querySelector('#grupo_telefono i').classList.add('fa-check-circle-o');
            document.querySelector('#grupo_telefono i').classList.remove('fa-times-circle');
            document.querySelector('#grupo_telefono .formulario__input-error').classList.remove('formulario__input-error-activo');

        } else{

            document.getElementById('grupo_telefono').classList.add('formulario__grupo-incorrecto');
            document.getElementById('grupo_telefono').classList.remove('formulario__grupo-correcto');
            document.querySelector('#grupo_telefono i').classList.add('fa-times-circle');
            document.querySelector('#grupo_telefono i').classList.remove('fa-check-circle');
            document.querySelector('#grupo_telefono .formulario__input-error').classList.add('formulario__input-error-activo');

        }
    break;

    }
}

inputs.forEach((input) => {

    input.addEventListener('keyup', validarFormulario);
    input.addEventListener('blur', validarFormulario);

})

formulario.addEventListener('submit', (e) => {

    e.preventDefault();

})

formulario.addEventListener('submit', (e) => {

    e.preventDefault();

})

const navToggle = document.querySelector(".nav_toggle")
const navMenu = document.querySelector(".nav_menu")

navToggle.addEventListener("click", () => {

    navMenu.classList.toggle("nav_menu_visible");

    if(navMenu.classList.contains("nav_menu_visible")){

        navToggle.setAttribute("aria-label", "Cerrar menú");

    } else {

        navToggle.setAttribute("aria-label", "Abrir menú");

    }

})