//VALIDACION DE DATOS formulario 1//

const formulario = document.getElementById('form');
const inputs = document.querySelectorAll('#form input');
const expresiones = {
	usuario: /^[a-zA-Z0-9\_\-]{4,16}$/, // Letras, numeros, guion y guion_bajo
	nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
	ciudad: /^[a-zA-ZÀ-ÿ\s]{1,50}$/, // Letras y espacios, pueden llevar acentos.
	edad: /^.{1,2}$/, // 1 a 2 digitos.
	matricula: /^.{12,12}$/, // 12 digitos.
	curso:/^.{1,2}$/, // 1 a 2 digitos.
	telefono: /^.{10,10}$/, // 10 digitos.
	correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
}

const validarFormulario = (e) =>{

    switch (e.target.name){

        case "nombre":
            if(expresiones.nombre.test(e.target.value)){

                document.getElementById('grupo__nombre').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo__nombre').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo__nombre i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo__nombre i').classList.remove('fa-times-circle');
                document.querySelector('#grupo__nombre .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo__nombre').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo__nombre').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo__nombre i').classList.add('fa-times-circle');
                document.querySelector('#grupo__nombre i').classList.remove('fa-check-circle');
                document.querySelector('#grupo__nombre .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;
        case "apellido":
            if(expresiones.nombre.test(e.target.value)){

                document.getElementById('grupo__apellido').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo__apellido').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo__apellido i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo__apellido i').classList.remove('fa-times-circle');
                document.querySelector('#grupo__apellido .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo__apellido').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo__apellido').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo__apellido i').classList.add('fa-times-circle');
                document.querySelector('#grupo__apellido i').classList.remove('fa-check-circle');
                document.querySelector('#grupo__apellido .formulario__input-error').classList.add('formulario__input-error-activo');

            }
		break;
		case "edad":
            if(expresiones.edad.test(e.target.value)){

                document.getElementById('grupo__edad').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo__edad').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo__edad i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo__edad i').classList.remove('fa-times-circle');
                document.querySelector('#grupo__edad .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo__edad').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo__edad').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo__edad i').classList.add('fa-times-circle');
                document.querySelector('#grupo__edad i').classList.remove('fa-check-circle');
                document.querySelector('#grupo__edad .formulario__input-error').classList.add('formulario__input-error-activo');

            }
		break;
		case "correo":
            if(expresiones.correo.test(e.target.value)){

                document.getElementById('grupo__correo').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo__correo').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo__correo i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo__correo i').classList.remove('fa-times-circle');
                document.querySelector('#grupo__correo .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo__correo').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo__correo').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo__correo i').classList.add('fa-times-circle');
                document.querySelector('#grupo__correo i').classList.remove('fa-check-circle');
                document.querySelector('#grupo__correo .formulario__input-error').classList.add('formulario__input-error-activo');

            }
		break;
		case "sangre":
			var sangre=document.getElementById('sangre').value;
            if(sangre.trim("")){

                document.getElementById('grupo__sangre').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo__sangre').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo__sangre i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo__sangre i').classList.remove('fa-times-circle');
                document.querySelector('#grupo__sangre .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo__sangre').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo__sangre').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo__sangre i').classList.add('fa-times-circle');
                document.querySelector('#grupo__sangre i').classList.remove('fa-check-circle');
                document.querySelector('#grupo__sangre .formulario__input-error').classList.add('formulario__input-error-activo');

            }
		break;
		case "ciudad":
            if(expresiones.ciudad.test(e.target.value)){

                document.getElementById('grupo__ciudad').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo__ciudad').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo__ciudad i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo__ciudad i').classList.remove('fa-times-circle');
                document.querySelector('#grupo__ciudad .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo__ciudad').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo__ciudad').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo__ciudad i').classList.add('fa-times-circle');
                document.querySelector('#grupo__ciudad i').classList.remove('fa-check-circle');
                document.querySelector('#grupo__ciudad .formulario__input-error').classList.add('formulario__input-error-activo');

            }
		break;
		case "direccion":
            if(expresiones.ciudad.test(e.target.value)){

                document.getElementById('grupo__direccion').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo__direccion').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo__direccion i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo__direccion i').classList.remove('fa-times-circle');
                document.querySelector('#grupo__direccion .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo__direccion').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo__direccion').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo__direccion i').classList.add('fa-times-circle');
                document.querySelector('#grupo__direccion i').classList.remove('fa-check-circle');
                document.querySelector('#grupo__direccion .formulario__input-error').classList.add('formulario__input-error-activo');

            }
		break;
		case "telefono":
            if(expresiones.telefono.test(e.target.value)){

                document.getElementById('grupo__telefono').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo__telefono').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo__telefono i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo__telefono i').classList.remove('fa-times-circle');
                document.querySelector('#grupo__telefono .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo__telefono').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo__telefono').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo__telefono i').classList.add('fa-times-circle');
                document.querySelector('#grupo__telefono i').classList.remove('fa-check-circle');
                document.querySelector('#grupo__telefono .formulario__input-error').classList.add('formulario__input-error-activo');

            }
		break;
		case "ocupacion":
            if(expresiones.nombre.test(e.target.value)){

                document.getElementById('grupo__ocupacion').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo__ocupacion').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo__ocupacion i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo__ocupacion i').classList.remove('fa-times-circle');
                document.querySelector('#grupo__ocupacion .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo__ocupacion').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo__ocupacion').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo__ocupacion i').classList.add('fa-times-circle');
                document.querySelector('#grupo__ocupacion i').classList.remove('fa-check-circle');
                document.querySelector('#grupo__ocupacion .formulario__input-error').classList.add('formulario__input-error-activo');

            }
		break;
		case "facultad":
            if(expresiones.nombre.test(e.target.value)){

                document.getElementById('grupo__facultad').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo__facultad').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo__facultad i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo__facultad i').classList.remove('fa-times-circle');
                document.querySelector('#grupo__facultad .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo__facultad').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo__facultad').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo__facultad i').classList.add('fa-times-circle');
                document.querySelector('#grupo__facultad i').classList.remove('fa-check-circle');
                document.querySelector('#grupo__facultad .formulario__input-error').classList.add('formulario__input-error-activo');

            }
		break;
		case "carrera":
            if(expresiones.nombre.test(e.target.value)){

                document.getElementById('grupo__carrera').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo__carrera').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo__carrera i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo__carrera i').classList.remove('fa-times-circle');
                document.querySelector('#grupo__carrera .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo__carrera').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo__carrera').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo__carrera i').classList.add('fa-times-circle');
                document.querySelector('#grupo__carrera i').classList.remove('fa-check-circle');
                document.querySelector('#grupo__carrera .formulario__input-error').classList.add('formulario__input-error-activo');

            }
		break;
		case "curso":
            if(expresiones.curso.test(e.target.value)){

                document.getElementById('grupo__curso').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo__curso').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo__curso i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo__curso i').classList.remove('fa-times-circle');
                document.querySelector('#grupo__curso .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo__curso').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo__curso').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo__curso i').classList.add('fa-times-circle');
                document.querySelector('#grupo__curso i').classList.remove('fa-check-circle');
                document.querySelector('#grupo__curso .formulario__input-error').classList.add('formulario__input-error-activo');

            }
		break;
		case "matricula":
            if(expresiones.matricula.test(e.target.value)){

                document.getElementById('grupo__matricula').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo__matricula').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo__matricula i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo__matricula i').classList.remove('fa-times-circle');
                document.querySelector('#grupo__matricula .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo__matricula').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo__matricula').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo__matricula i').classList.add('fa-times-circle');
                document.querySelector('#grupo__matricula i').classList.remove('fa-check-circle');
                document.querySelector('#grupo__matricula .formulario__input-error').classList.add('formulario__input-error-activo');

            }
		break;


    }
}

inputs.forEach((input) => {

    input.addEventListener('keyup', validarFormulario);
    input.addEventListener('blur', validarFormulario);

})

formulario.addEventListener('submit', (e) => {

    e.preventDefault();

})

formulario.addEventListener('submit', (e) => {

    e.preventDefault();

})