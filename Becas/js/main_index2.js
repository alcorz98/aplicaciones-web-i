const formulario2 = document.getElementById('form2');
const inputs2 = document.querySelectorAll('#form2 input');
const expresiones2 = {
	usuario: /^[a-zA-Z0-9\_\-]{4,16}$/, // Letras, numeros, guion y guion_bajo
	nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
	password: /^.{4,12}$/, // 4 a 12 digitos.
	correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
}

const validarFormulario2 = (e) =>{

    switch (e.target.name){
        
        case "usuario2":
            if(expresiones2.usuario.test(e.target.value)){

                document.getElementById('grupo_usuario2').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_usuario2').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo_usuario2 i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo_usuario2 i').classList.remove('fa-times-circle');
                document.querySelector('#grupo_usuario2 .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_usuario2').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_usuario2').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_usuario2 i').classList.add('fa-times-circle');
                document.querySelector('#grupo_usuario2 i').classList.remove('fa-check-circle');
                document.querySelector('#grupo_usuario2 .formulario__input-error').classList.add('formulario__input-error-activo');

            }
            
        break;
        case "correo":
            if(expresiones2.correo.test(e.target.value)){

                document.getElementById('grupo_correo').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_correo').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo_correo i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo_correo i').classList.remove('fa-times-circle');
                document.querySelector('#grupo_correo .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_correo').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_correo').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_correo i').classList.add('fa-times-circle');
                document.querySelector('#grupo_correo i').classList.remove('fa-check-circle');
                document.querySelector('#grupo_correo .formulario__input-error').classList.add('formulario__input-error-activo');

            }
            
        break;
        case "contraseña2":
            if(expresiones2.password.test(e.target.value)){

                document.getElementById('grupo_contraseña2').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_contraseña2').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo_contraseña2 i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo_contraseña2 i').classList.remove('fa-times-circle');
                document.querySelector('#grupo_contraseña2 .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_contraseña2').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_contraseña2').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_contraseña2 i').classList.add('fa-times-circle');
                document.querySelector('#grupo_contraseña2 i').classList.remove('fa-check-circle');
                document.querySelector('#grupo_contraseña2 .formulario__input-error').classList.add('formulario__input-error-activo');

            }
            
        break;
        case "contraseña_conf":
            if(document.getElementById('contraseña2').value == document.getElementById('contraseña_conf').value){

                document.getElementById('grupo_contraseña2_conf').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_contraseña2_conf').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo_contraseña2_conf i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo_contraseña2_conf i').classList.remove('fa-times-circle');
                document.querySelector('#grupo_contraseña2_conf .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_contraseña2_conf').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_contraseña2_conf').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_contraseña2_conf i').classList.add('fa-times-circle');
                document.querySelector('#grupo_contraseña2_conf i').classList.remove('fa-check-circle');
                document.querySelector('#grupo_contraseña2_conf .formulario__input-error').classList.add('formulario__input-error-activo');

            }
            
        break;

    }
}

inputs2.forEach((input) => {

    input.addEventListener('keyup', validarFormulario2);
    input.addEventListener('blur', validarFormulario2);

})

formulario2.addEventListener('submit', (e) => {

    e.preventDefault();

})

document.querySelector('#enviar').addEventListener('click', 
enviar);

function enviar(){
    window.location= "inicio.html";
}