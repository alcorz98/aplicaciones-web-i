const express = require('express');
const router = express.Router();

const passport = require('passport');
const { isLoggedIn, isNotLoggedIn } = require('../lib/auth');

router.get('/registro', isNotLoggedIn, (req, res) => {
    res.render('auth/registro');
});

router.post('/registro', isNotLoggedIn, passport.authenticate('local.registro', {
    successRedirect: '/profile',
    failureRedirect: '/registro',
    failureFlash: true
}));

module.exports = router;