const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const pool = require('../database');
const helpers = require('../lib/helpers');

passport.use('local.registro', new LocalStrategy({

    nameField: 'fullname',
    correoField: 'email',
    passReqToCallback: true

}, async(req, fullname, email, done) => {

    const { cedula } = req.body;
    const { direccion } = req.body;
    const { telefono } = req.body;

    const newUser = {

        cedula,
        fullname,
        email,
        direccion,
        telefono

    };
    const result = await pool.query('INSERT INTO estudiantes SET ?', [newUser]);
    newUser.id = result.insertId;
    return done(null, newUser);
}));

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser(async(id, done) => {
    const rows = await pool.query('SELECT * FROM users WHERE id = ?', [id]);
    done(null, rows[0]);
});