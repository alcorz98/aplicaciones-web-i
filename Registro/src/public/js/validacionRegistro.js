//Expresiones regulares
const expresiones = {
    cedula: /^\d{10}$/, // numérico de 10 dígitos.
    nombres: /^[a-zA-ZÀ-ÿ\s]{1,80}$/, //Texto de al menos 80 digitos.
    direccion: /^([a-zA-Z0-9_\s]){1,80}$/, //Alfanumerico de al menos 80 digitos.
    telefono: /^\d{10}$/, // numérico de 10 dígitos.
    correo: /\S+@\S+.\S+/, // formato valido de correo.
};

function revisar(elemento) {
    if (elemento.value == '') {
        elemento.className = 'error';
    } else {
        elemento.className = 'input';
    }
}

function revisarEmail(elemento) {
    if (elemento.value !== '') {
        var data = elemento.value;
        if (!expresiones.correo.test(data)) {
            elemento.className = 'error';
        } else {
            elemento.className = 'input';
        }
    }
}

function revisarCedula(elemento) {
    if (elemento.value !== '') {
        var data = elemento.value;
        if (!expresiones.cedula.test(data)) {
            elemento.className = 'error';
        } else {
            elemento.className = 'input';
        }
    }
}

function revisarNombre(elemento) {
    if (elemento.value !== '') {
        var data = elemento.value;
        if (!expresiones.nombre.test(data)) {
            elemento.className = 'error';
        } else {
            elemento.className = 'input';
        }
    }
}

function revisarTelefono(elemento) {
    if (elemento.value !== '') {
        var data = elemento.value;
        if (!expresiones.telefono.test(data)) {
            elemento.className = 'error';
        } else {
            elemento.className = 'input';
        }
    }
}

function revisarDireccion(elemento) {
    if (elemento.value !== '') {
        var data = elemento.value;
        if (!expresiones.direccion.test(data)) {
            elemento.className = 'error';
        } else {
            elemento.className = 'input';
        }
    }
}

function validar() {
    var datosCorrectos = true;
    var error1 = "";
    var error2 = "";
    var error3 = "";
    var error4 = "";
    var error5 = "";

    if (document.getElementById("cedula").value == "" || !expresiones.cedula.test(document.getElementById("cedula").value)) {
        datosCorrectos = false;
        error1 = "\nIngrese cédula correcta"
    }

    if (document.getElementById("fullname").value == "" || !expresiones.nombres.test(document.getElementById("fullname").value)) {
        datosCorrectos = false;
        error2 = "\nINgrese un nombre correcto"
    }

    if (document.getElementById("email").value == "" || !expresiones.email.test(document.getElementById("email").value)) {
        datosCorrectos = false;
        error3 = "\nEmail inválido"
    }

    if (document.getElementById("direccion").value == "" || !expresiones.direccion.test(document.getElementById("direccion").value)) {
        datosCorrectos = false;
        error4 = "\nDireccion inválida"
    }

    if (document.getElementById("telefono").value == "" || !expresiones.telefono.test(document.getElementById("telefono").value)) {
        datosCorrectos = false;
        error4 = "\nDireccion inválida"
    }

    if (!datosCorrectos) {
        alert('Hay errores en el formulario' + error1 + error2 + error3 + error4 + error5);
    }

    return datosCorrectos;
}