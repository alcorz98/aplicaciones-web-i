const formulario = document.getElementById('form');
const inputs = document.querySelectorAll('#form input');
const expresiones = {
	marca: /^[a-zA-Z0-9]{1,50}$/, // Letras, numeros.
    codigo: /^[a-zA-Z0-9]{1,5}$/, // Letras, numeros.
    modelo: /^[a-zA-Z0-9]{1,30}$/, // Letras, numeros.
    año: /^\d{1,4}$/, // 1 a 4 numeros.
}

const validarFormulario = (e) =>{

    switch (e.target.name){

        case "codigo":
            if(expresiones.codigo.test(e.target.value)){

                document.getElementById('grupo_codigo').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_codigo').classList.add('formulario__grupo-correcto')
                document.querySelector('#grupo_codigo .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_codigo').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_codigo').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_codigo .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;

        case "marca":
            if(expresiones.marca.test(e.target.value)){

                document.getElementById('grupo_marca').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_marca').classList.add('formulario__grupo-correcto')
                document.querySelector('#grupo_marca .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_marca').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_marca').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_marca .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;

        case "modelo":
            if(expresiones.modelo.test(e.target.value)){

                document.getElementById('grupo_modelo').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_modelo').classList.add('formulario__grupo-correcto')
                document.querySelector('#grupo_modelo .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_modelo').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_modelo').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_modelo .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;

        case "año":
            if(expresiones.año.test(e.target.value)){

                document.getElementById('grupo_año').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_año').classList.add('formulario__grupo-correcto')
                document.querySelector('#grupo_año .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_año').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_año').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_año .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;

        case "fecha_inicial":
            if(document.getElementById('fecha_inicial').value == ""){

                document.getElementById('grupo_fecha_inicial').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_fecha_inicial').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_fecha_inicial .formulario__input-error').classList.add('formulario__input-error-activo');
                

            } else {

                document.getElementById('grupo_fecha_inicial').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_fecha_inicial').classList.add('formulario__grupo-correcto')
                document.querySelector('#grupo_fecha_inicial .formulario__input-error').classList.remove('formulario__input-error-activo');

            }
        
        case "fecha_final":
            if(document.getElementById('fecha_final').value > document.getElementById('fecha_inicial').value){

                document.getElementById('grupo_fecha_final').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_fecha_final').classList.add('formulario__grupo-correcto')
                document.querySelector('#grupo_fecha_final .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else {

                document.getElementById('grupo_fecha_final').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_fecha_final').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_fecha_final .formulario__input-error').classList.add('formulario__input-error-activo');

            }

    }
}

inputs.forEach((input) => {

    input.addEventListener('keyup', validarFormulario);
    input.addEventListener('blur', validarFormulario);

})

formulario.addEventListener('submit', (e) => {

    e.preventDefault();

})