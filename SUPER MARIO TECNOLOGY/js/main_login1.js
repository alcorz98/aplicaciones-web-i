//ANIMACION//
const inicio_sesion_boton = document.querySelector("#boton_inicio");
const registro_boton = document.querySelector("#boton_registro");
const contenedor = document.querySelector(".contenedor");

registro_boton.addEventListener('click', () =>{

    contenedor.classList.add("registro_modo");

});

inicio_sesion_boton.addEventListener('click', () =>{

    contenedor.classList.remove("registro_modo");

});

//VALIDACION DE DATOS formulario 1//

const formulario = document.getElementById('form');
const inputs = document.querySelectorAll('#form input');
const expresiones = {
	usuario: /^[a-zA-Z0-9\_\-]{4,16}$/, // Letras, numeros, guion y guion_bajo
	nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
	password: /^.{4,12}$/, // 4 a 12 digitos.
	correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
}

const validarFormulario = (e) =>{

    switch (e.target.name){

        case "usuario":
            if(expresiones.usuario.test(e.target.value)){

                document.getElementById('grupo_usuario').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_usuario').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo_usuario i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo_usuario i').classList.remove('fa-times-circle');
                document.querySelector('#grupo_usuario .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_usuario').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_usuario').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_usuario i').classList.add('fa-times-circle');
                document.querySelector('#grupo_usuario i').classList.remove('fa-check-circle');
                document.querySelector('#grupo_usuario .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;
        case "contraseña":
            if(expresiones.password.test(e.target.value)){

                document.getElementById('grupo_contraseña').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_contraseña').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo_contraseña i').classList.add('fa-check-circle-o');
                document.querySelector('#grupo_contraseña i').classList.remove('fa-times-circle');
                document.querySelector('#grupo_contraseña .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_contraseña').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_contraseña').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_contraseña i').classList.add('fa-times-circle');
                document.querySelector('#grupo_contraseña i').classList.remove('fa-check-circle');
                document.querySelector('#grupo_contraseña .formulario__input-error').classList.add('formulario__input-error-activo');

            }

    }
}

inputs.forEach((input) => {

    input.addEventListener('keyup', validarFormulario);
    input.addEventListener('blur', validarFormulario);

})

formulario.addEventListener('submit', (e) => {

    e.preventDefault();

})

formulario.addEventListener('submit', (e) => {

    e.preventDefault();

})

//Ingreso

function ir(){

    var form = document.form;
    var user = form.usuario.value;
    var pass = form.contraseña.value;

    if(user == "admin" && pass == "123polito"){

        window.location = "index.html";

    } else {

        document.getElementById("a-error").innerHTML = "Usuario y contraseña incorrectos.";

    }

}

