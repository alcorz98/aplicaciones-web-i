document.querySelector('#btn').addEventListener('click', traerJson);

function traerJson(){
    const json = new XMLHttpRequest();

    json.open('GET', 'js/estudiantes.json', true);
    json.send();
    json.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            let datos = JSON.parse(this.responseText);
            let res = document.querySelector('#resultado');
            res.innerHTML = '';

            for(let item of datos){
                res.innerHTML += `
                
                    <tr>
                        <td>${item.CEDULA}</td>
                        <td>${item.NOMBRE}</td>
                        <td>${item.DIRECCION}</td>
                        <td>${item.TELEFONO}</td>
                        <td>${item.CORREO}</td>
                        <td>${item.CURSO}</td>
                        <td>${item.PARALELO}</td>
                    </tr>
                
                `
            }
        }
    }
}