const express = require('express');
const router = express.Router();

router.get('/', (req, res) =>{
    res.render('index')
});

router.get('/becas', (req, res) =>{
    res.render('becas')
});

router.get('/contacto', (req, res) =>{
    res.render('contacto')
});

module.exports = router;