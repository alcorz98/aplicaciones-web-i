const express = require('express');
const router = express.Router();

const passport = require('passport');
const {isLoggedIn, isNotLoggedIn} = require('../lib/auth');

router.get('/registro', isNotLoggedIn, (req, res) => {
    res.render('auth/registro');
});

router.post('/registro', isNotLoggedIn, passport.authenticate('local.registro', {
    successRedirect: '/profile',
    failureRedirect: '/registro',
    failureFlash: true
}));

router.get('/login', isNotLoggedIn, (req, res) => {
    res.render('auth/login');
});

router.post('/login', isNotLoggedIn, (req, res, next) => {
    passport.authenticate('local.login', {
        successRedirect: '/profile',
        failureRedirect: '/registro',
        failureFlash: true
    })(req, res, next);
});

router.get('/profile', isLoggedIn, (req, res) => {
    res.render('profile');
});

router.get('/logout', (req, res) => {
    req.logOut();
    res.redirect('/login');
})

module.exports = router;