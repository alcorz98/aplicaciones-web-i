const formulario = document.getElementById('form');
const inputs = document.querySelectorAll('#form input');
const expresiones = {
	usuario: /^[a-zA-Z0-9\_\-]{4,16}$/, // Letras, numeros, guion y guion_bajo
    password: /^.{4,12}$/ // 4 a 12 digitos.
}

const validarFormulario = (e) =>{

    switch (e.target.name){

        case "username":
            if(expresiones.usuario.test(e.target.value)){

                document.getElementById('grupo_usuario').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_usuario').classList.add('formulario__grupo-correcto')
                document.querySelector('#grupo_usuario .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_usuario').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_usuario').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_usuario .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;

        case "password":
            if(expresiones.password.test(e.target.value)){

                document.getElementById('grupo_password').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_password').classList.add('formulario__grupo-correcto')
                document.querySelector('#grupo_password .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_password').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_password').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_password .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;

    }
}

inputs.forEach((input) => {

    input.addEventListener('keyup', validarFormulario);
    input.addEventListener('blur', validarFormulario);

})