const formulario = document.getElementById('form');
const inputs = document.querySelectorAll('#form input');
const expresiones = {
	nombre: /^[a-zA-ZÀ-ÿ\s]{1,50}$/, // Letras y espacios, pueden llevar acentos.
    usuario: /^[a-zA-Z0-9\_\-]{4,16}$/, // Letras, numeros, guion y guion_bajo
    password: /^.{4,12}$/, // 4 a 12 digitos.
    email: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
}

const validarFormulario = (e) =>{

    switch (e.target.name){

        case "fullname":
            if(expresiones.nombre.test(e.target.value)){

                document.getElementById('grupo_nombre').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_nombre').classList.add('formulario__grupo-correcto')
                document.querySelector('#grupo_nombre .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_nombre').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_nombre').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_nombre .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;

        case "username":
            if(expresiones.usuario.test(e.target.value)){

                document.getElementById('grupo_usuario').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_usuario').classList.add('formulario__grupo-correcto')
                document.querySelector('#grupo_usuario .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_usuario').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_usuario').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_usuario .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;

        case "email":
            if(expresiones.email.test(e.target.value)){

                document.getElementById('grupo_email').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_email').classList.add('formulario__grupo-correcto')
                document.querySelector('#grupo_email .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_email').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_email').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_email .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;

        case "password":
            if(expresiones.password.test(e.target.value)){

                document.getElementById('grupo_password').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_password').classList.add('formulario__grupo-correcto')
                document.querySelector('#grupo_password .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_password').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_password').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_password .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;

    }
}

inputs.forEach((input) => {

    input.addEventListener('keyup', validarFormulario);
    input.addEventListener('blur', validarFormulario);

})