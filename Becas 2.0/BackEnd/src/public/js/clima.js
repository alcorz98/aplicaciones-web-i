link = "https://api.openweathermap.org/data/2.5/weather?q=Manta&appid=76e23c5df75ef6522bd880f46a404ad9";

var request = new XMLHttpRequest();
request.open('GET', link, true);
request.onload = function() {
    var obj = JSON.parse(this.response);
    console.log(obj);
    const tiempo = obj.main.temp - 273.15;
    document.getElementById('weather').innerHTML = obj.weather[0].description;
    document.getElementById('location').innerHTML = " " + obj.name;
    document.getElementById('temp').innerHTML = tiempo.toFixed(2) + " °";
    document.getElementById('icon').src = "http://openweathermap.org/img/w/" + obj.weather[0].icon + ".png";
}
if (request.status == 200) {
    console.log("Error");
}
request.send();