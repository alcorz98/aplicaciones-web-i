const formulario = document.getElementById('form');
const inputs = document.querySelectorAll('#form input');
const expresiones = {
	nombre: /^[a-zA-ZÀ-ÿ\s]{1,50}$/, // Letras y espacios, pueden llevar acentos.
    telefono: /^.{1,13}$/, // 1 a 13 digitos.
    direccion:/^[a-zA-Z0-ZÀ-ÿ-9\s\_\-]{1,80}$/, // Letras, numeros, ,espacios, guion y guion_bajo
    email: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
}

const validarFormulario = (e) =>{

    switch (e.target.name){

        case "nombre":
            if(expresiones.nombre.test(e.target.value)){

                document.getElementById('grupo_nombre').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_nombre').classList.add('formulario__grupo-correcto')
                document.querySelector('#grupo_nombre .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_nombre').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_nombre').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_nombre .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;

        case "telefono":
            if(expresiones.telefono.test(e.target.value)){

                document.getElementById('grupo_telefono').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_telefono').classList.add('formulario__grupo-correcto')
                document.querySelector('#grupo_telefono .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_telefono').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_telefono').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_telefono .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;

        case "direccion":
            if(expresiones.direccion.test(e.target.value)){

                document.getElementById('grupo_direccion').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_direccion').classList.add('formulario__grupo-correcto')
                document.querySelector('#grupo_direccion .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_direccion').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_direccion').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_direccion .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;

        case "email":
            if(expresiones.email.test(e.target.value)){

                document.getElementById('grupo_email').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_email').classList.add('formulario__grupo-correcto')
                document.querySelector('#grupo_email .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_email').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_email').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_email .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;

    }
}

inputs.forEach((input) => {

    input.addEventListener('keyup', validarFormulario);
    input.addEventListener('blur', validarFormulario);

})
