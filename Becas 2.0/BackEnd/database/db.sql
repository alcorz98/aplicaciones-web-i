--Users Table
CREATE DATABASE database_links;

USE database_links;

CREATE TABLE users(
    id INT(11) NOT NULL,
    username VARCHAR(16) NOT NULL,
    password VARCHAR(60) NOT NULL,
    fullname VARCHAR(100) NOT NULL,
    email VARCHAR(50) NOT NULL
);

ALTER TABLE users 
    ADD PRIMARY KEY (id);

ALTER TABLE users
    MODIFY id INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 2;

DESCRIBE users;

--Links Table--
CREATE TABLE links (
    id INT(11) NOT NULL,
    nombre VARCHAR(50) NOT NULL,
    apellido VARCHAR(50) NOT NULL,
    fecha DATE NOT NULL, 
    edad INT(2) NOT NULL,
    correo VARCHAR(50) NOT NULL,
    sangre VARCHAR(3) NOT NULL,
    sexo VARCHAR(50) NOT NULL,
    ciudad VARCHAR(50) NOT NULL,
    direccion VARCHAR(50) NOT NULL,
    telefono INT(10) NOT NULL,
    ocupacion VARCHAR(50) NOT NULL,
    estado VARCHAR(40) NOT NULL,
    matriz VARCHAR(50) NOT NULL,
    facultad VARCHAR(50) NOT NULL,
    carrera VARCHAR(50) NOT NULL,
    curso VARCHAR(2) NOT NULL,
    matricula VARCHAR(20) NOT NULL,
    user_id INT(11),
    create_at timestamp NOT NULL DEFAULT current_timestamp,
    CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users(id)

);

ALTER TABLE links
    ADD PRIMARY KEY (id);

ALTER TABLE links
    MODIFY id INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 2;

DESCRIBE links;